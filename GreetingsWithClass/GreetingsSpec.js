
class Robot {
  constructor() {
    this.aName = 'my friend'
    this.anotherName = null
  }

  meet(someone) {
    if (this.aName === 'my friend') {
      this.aName = someone
    } else {
      this.anotherName = someone
    }
  }

  greet() {
    if (this.anotherName === null) {
      return `Hello, ${this.aName}.`
    } else {
      return `Hello, ${this.aName} and ${this.anotherName}.`
    }
  }
}

describe('Greetings', () => {
  it('greets to Bob', () => {
    const robot = new Robot()

    robot.meet('Bob')

    const greet = robot.greet()
    expect(greet).toBe('Hello, Bob.')
  })

  it('greets to someone', () => {
    const robot = new Robot()

    robot.meet('Akira')

    expect(robot.greet()).toBe('Hello, Akira.')
  })

  it('greets to a friend when does not meet anyone', () => {
    const robot = new Robot()

    const greet = robot.greet()

    expect(greet).toBe('Hello, my friend.')
  })

  it('greets to known people', () => {
    const robot = new Robot()

    robot.meet('Charlie')
    robot.meet('Akira')

    expect(robot.greet()).toBe('Hello, Charlie and Akira.')
  })
})
