function numberFizzBuzz(number) {
    if(number % 3 === 0 && number % 5 === 0){
        return 'FizzBuzz'
    } 
    if(number % 3 === 0){
        return 'Fizz'
    } 
    if(number % 5 === 0){
        return 'Buzz'
    }
    return '' + number
    // return number.toString()
}

describe("FizzBuzz",()=> {
    it('returns if number is Fizz',()=>{
        const numberFizz = 3

        const result = numberFizzBuzz(numberFizz)

        expect(result).toBe('Fizz')
    })

    it('returns if number is Buzz', ()=>{
        const numberBuzz = 5

        const result = numberFizzBuzz(numberBuzz)

        expect(result).toBe('Buzz')

    })

    it('returns FizzBuzz if numbers are Fizz and Buzz',()=>{
        const aNumberFizzBuzz = 15

        const result = numberFizzBuzz(aNumberFizzBuzz)

        expect(result).toBe('FizzBuzz')
    })

    it('returns a number if numbers are not Fizz, Buzz or FizzBuzz', () =>{
        const numberNotFizzBuzz = 2

        const result = numberFizzBuzz(numberNotFizzBuzz)

        expect(result).toBe('2')
    })

})