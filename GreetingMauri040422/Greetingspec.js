describe('Greeting', () => {
    it('greets with the given name', () => {
        // arrange (preparación)
        const name = 'Bob'

        //act (utilizar la función)
        const greeting = greet(name)

        //assert (lo que esperamos)
        expect(greeting).toBe('Hello, Bob.')
    })

    it('greets with a different given name', () => {
        const name = 'Heidi'

        const greeting = greet(name)

        expect(greeting).toBe('Hello, Heidi.')
    })

    it('greets with a standard salute when no name is given', () => {
        const noName = ''

        const greeting = greet(noName)

        expect(greeting).toBe("Hello, my friend.")
    })

    it('shouts if the name is all uppercase', () => {
        const shoutedName = 'JERRY'

        const greeting = greet(shoutedName)

        expect(greeting).toBe("HELLO JERRY!")
    })
})

function greet(name) {
    const predefinedGreet = "Hello, my friend."

    if (isEmpty(name)) {
        return predefinedGreet
    }
    if (isUpperCase(name)) {
        return `HELLO ${name}!`
    } else {
        return `Hello, ${name}.`
    }
}

function isEmpty(text) {
    return (text === '')
}

function isUpperCase(text) {
    return (text.toUpperCase() === text)
}