# Apuntes

## Declaración de variables
Las variables son contenedores de información.

Cuando el valor de la variable va a cambiar utilizamos `let`:
```js

let nombreDeLaVariable = 'valor que va almacenar la variable'

```

Cuando el valor no va a cambiar después de definir la variable utilizamos `const`:
```js

const nombreDeLaVariable = 'valor que va almacenar la variable'

```

No utilizar `var` para declarar variables:
```js

var nombreDeLaVariable = 'valor que va almacenar la variable'

```

### Funciones
Una **función** es una sección de un programa que calcula un valor de manera independiente al resto del programa.

Una función tiene tres componentes importantes:

- Los **parámetros**, que son los valores de entrada que recibe la función, que son opcionales
- El **código de la función**, que son las operaciones que hace la función
- El **resultado** (o **valor de retorno**), que es el valor final que entrega la función, que es opcional.
Existen 3 maneras de definir una función:

#### 1- Asignado a una variable la función:
```js
let nombreFuncion = function(parametro1, parametro2){
	codigo de la función
	return "valor de vuelta"
}
```

#### 2 - **fat arrows functions** solo se diferencia del anterior en que no lleva la palabra `function`
```js
let nombreFuncion = (parametro1, parametro2) =>{
	codigo de la función
	return "valor de vuelta"
}
```

En el caso de solo pasarle un `parametro` no es **necesario** poner los `(parametro)`
```js
let nombreFuncion = parametro =>{
	codigo de la función
	return "valor de vuelta"
}
```

#### 3- Sin asignársela a una variable:
```js
function nombreFuncion(parametro1, parametro2){
	codigo de la función
	return "valor de vuelta"
}
```

El `let` se puede sustituir por `const` o `var` en los casos que sea necesario.

El codigo que hay dentro de una función, **no se ejecuta** mientras no sea llamada la función:
```js
nombreFuncion(parametro1, parametro2)
```

Las funciones cuando contienen definido un `return`, es que devuelven un `valor`, que puede ser almacenado en una variable
```js
function nombreFuncion(parametro1, parametro2){
	codigo de la función
	return "valor de vuelta"
}
let resultado = nombreFuncio(parametro1, parametro2)
```

El contenido de la variable seria "valor de vuelta"

## Condicionales (if y ternarios)
Los condicionales nos permiten tomar decisiones, evaluando si una expresión es cierta o falsa.

### if
Si se cumple la condición del if se ejecuta el código que hay entre las `{ }`, si no se cumple, no se ejecuta ese código que hay entre `{ }`.
```js
if(expresionAEvaluar){
	Código a ejecutar
}
```

Existe otra manera de que se ejecute código sin que éste entre `{}`, que es ponerlo en una sola linea
```js
if(expresiónAEvaluar) código a ejecutar
```

Tambien se puede definir el codigo que queremos que se ejecute si no se cumple la condición con `else`.

Si no se cumple la condicion que hay definida se ejecuta el codigo que hay entre `{ }` despues del `else`.
```js
if(expresionAEvaluar){
	Código a ejecutar
} else {
	Código a ejecutar si no se cumple la condición
}
```

### ternarios
La estructura de este condicional es la siguiente
```js
expresiónAEvaluar ? seJecutaSiEscierto : seJecutaSiNOEscierto
```
No son aconsejables por que su lectura puede ser muy engorrosa.

## Bucles
Son estructuras de código que se ejcutan mientras se cumple una condición.
### while
```js
while (expresionAEvaluar) {
	Codigo a ejecutar
}
```

Hay que tener encuenta que la `expresionAEvaluar` se tiene que modificar dentro del bucle para que en algún momento se de la condición de salida, sino se convierte en un bucle infinito.

Por ejemplo :
```js
let numero = 1
while (nuemero === 1){
	Codigo a ejecutar
}
```
Si no cambiamos el valor de numero por un valor diferente a `1` nunca se saldra del bucle.

### for
En el bucle for, a diferencia del `while`, la condición de salida está definida en la misma linea del `for`.

La declaración se divide en 3 partes separadas por `;`
> - `let index = 0` inicializamos la variable `index` a `0`
>- `index < 10` condición que se tiene que **dejar de cumplir** para, salir del bucle `for` en este caso la condición de salida es que `index` sea mayor o igual a `10`.
> - `index++` define que `index` se irá incrementando en 1, en cada una de las pasadas del bucle.

```js
for (let index = 0; index < 10; index++) {
	const element = array[index]
}
```

Esta era la manera habitual de recorrer `arrays` .

### Array.filter()
El método `filter()` crea un **nuevo** array con todos los elementos que cumplan la condición implementada por la función dada.
```js
const arrayDeNombres = ['Samuel', 'Monica', 'Pepe', 'Carlos', 'Mariano', 'Maria Isabel']

function nombresLargos(palabra){
	return (palabra.length > 5)
}

const result = arrayDeNombres.filter(nombresLargos)

console.log(result)
// resultado: Array ["Monica", "Carlos", "Mariano", "Maria Isabel"]

```

Tambien se puede definir la función `nombreLargo` dentro de la llamada de la función `filter` como una función anonima (sin nombre):
```js
const arrayDeNombres = ['Samuel', 'Monica', 'Pepe', 'Carlos', 'Mariano', 'Maria Isabel']

const result = arrayDeNombres.filter(function (palabra){
	return (palabra.length > 5)
});

console.log(result)
// resultado: Array ["Monica", "Carlos", "Mariano", "Maria Isabel"]
```

Tambien se puede utilizar una **fat arrows function**:
```js
const arrayDeNombres = ['Samuel', 'Monica', 'Pepe', 'Carlos', 'Mariano', 'Maria Isabel']

const result = arrayDeNombres.filter((palabra) =>{
	return (palabra.length > 5)
});

console.log(result)
// resultado: Array ["Monica", "Carlos", "Mariano", "Maria Isabel"]
```

