
describe('Fizzbuzz',()=>{
    it('returns Fizz if the given number is disible by 3', ()=>{
        //arrange
        const divisibleByThree = 9
        //act
        const result = fizzbuzz(divisibleByThree)
        //assert
        expect(result).toBe('Fizz')
    })

    it('returns Buzz if the given number is divisible by 5', ()=>{
        const divisibleByFive = 25

        const result = fizzbuzz(divisibleByFive)

        expect(result).toBe('Buzz')

    })
})

function fizzbuzz(number){
    if(isDivisibleByFive(number)){
        return 'Buzz'
    } else {
        return 'Fizz'
    }
}

function isDivisibleByFive(number){
    return (number % 5 === 0)
}