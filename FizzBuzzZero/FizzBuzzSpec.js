
function fizzBuzz(aNumber) {
  let translation = `${aNumber}`

  if (aNumber % 3 === 0) {
    translation = 'Fizz'
  }

  return translation
}

describe('fizzBuzz', () => {
  it('does not translates the number when is not a FizzBuzz number', () => {
    const nonFizzBuzzNumber = 1

    const translation = fizzBuzz(nonFizzBuzzNumber)

    expect(translation).toBe('1')
  })

  it('translates Fizz numbers to Fizz', () => {
    // Arrange
    const fizzNumber = 3
    // Action
    const translation = fizzBuzz(fizzNumber)
    // Assertion
    expect(translation).toBe('Fizz')
  })

  it('translates Fizz numbers to Fizz', () => {

    const translation = fizzBuzz(6)

    expect(translation).toBe('Fizz')
  })
})

// camelCase
// cadaPalabraEmpizaPorMayusculaMenosLaPrimera

// PascalCase
// IgualQueElCamelPeroLaPrimeraTambienVaEnMayusculas

// snake_case
// esto_seria_snake_case

// CONSTANTES
// ESTO_REPRESENTA_A_UNA_CONSTANTE
