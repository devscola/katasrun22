# Programación Orientada a Objetos
## Las clases
Las **clases** son las plantillas con las que crear **objetos** y sirven para representar conceptos o entidades. Suelen ser sustantivos o entidades como por ejemplo: perro, coche, usuario, etiqueta,...

En las **clases** se definen los **atributos** y **métodos** que posteriormente se utilizarán en los **objetos**.

> - **Los Atributos**  son las carecterísticas o estados del **objeto** creado.
> - **Los Métodos** son las acciones que puede realizar el **objeto**.

Existen dos formas de declarar clases
```js
const nombreDeLaClase = {
	atributo1: ""
	atributo2: ""
	metodo1: function(){
		...
	}
	metodo2: function(){
		...
	}
```

```js
class nombreDeLaClase {
	constructor(){
		this.atributo1
		this.atributo2
	}
	metodo1(){
		...
	}
	metodo2(){
		...
	}
}
```

## Objetos
A cada **objeto** creado desde una **clase** se le llama **instancia de la clase**. Para crear una  **instancia** se utiliza  **new** y el nombre de la **clase**:
```js
let miObjeto = new nombreDeLaClase()
```

**miobjeto** es una **instancia** de la clase **nombreDeLaClase**.

Las **clases** también pueden recibir parámetros cuando son **instanciados**

```js
class miClase {
	constructor(prametro1, parametro2){
		this.atributo1 = parametro1
		this.atributo2 = parametro2
	}

	metodo1(){
		...
	}
	metodo2(){
		...
	}
}

let miObjeto = new miClase(prametro1, parametro2)
```

La diferencia entre **objetos** de la misma clase **son** sus **atributos**.

## this
Dentro de las clases existen dos ambitos: **ámbito de la clase** y **ámbito del objeto**, para poder diferenciarlos se utiliza **this** que representa al **objeto** que se halla instanciado.

