function fizzbuzz(number) {
    if(isDivisibleByThreeAndFive(number)) {
        return 'FizzBuzz'
    } else {
        return 'Fizz'
    }
}

function isDivisibleByThreeAndFive(number) {
    return ((number % 3 === 0) && (number % 5 === 0))
}

// pruebas de git

describe('FizzBuzz Kata', () => {
    it('if the number is divisible by 3 and 5 returns fizzBuzz', () => {
        // Arrange
        const divisibleByThreeAndFive = 15
        // Act
        const result = fizzbuzz(divisibleByThreeAndFive)
        // Assert
        expect(result).toBe('FizzBuzz')
    })

    it('if the number is divisible by 3 return fizz', () => {
        // Arrange
        const divisibleByThree = 3
        // Act
        const result = fizzbuzz(divisibleByThree)
        // Assert
        expect(result).toBe('Fizz')
    })
})
// Fin kata